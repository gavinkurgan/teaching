# Teaching NGS #

# Overview #
This tutorial will hopefully help teach you multiple techniques in NGS sequence processing.

## Setting up your machine ##
### Creating an environment ###
First, please make sure you have some sort of linux environment and anaconda installed. From there run:

```
$ conda create -n ngs_teaching
```
This will create an anaconda environment in Python3 for you to install your programs in. Enviroment creation using anaconda is a powerful tool for those looking to create reproducible systems. It's also great for those not experts in installing software

Next you will need to jump into your created enviroment:
```
$ source activate ngs_teaching
```

Now you are inside the "ngs_teaching environment"

### Installing software within your environment ###

First you need to add a channel that has a lot of great bioinformatics software compiled. One good one is bioconda. To do this run:
```
$ conda config -add channels bioconda
```

Other channels can be added similarly, and all channels and downloadable software can be found on anaconda's website.

Next you will need install the software within bioconda into your environment. To do this run:
```
$ conda install --quiet --yes --channel bioconda \
biopython=1.69 \
bwa=0.7.17 \
gatk4=4.0b6 \
picard=2.17.2 \
samtools=1.6-0 \
trim-galore= 0.4.5 \
```